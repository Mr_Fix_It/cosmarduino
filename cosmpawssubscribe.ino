


/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 18 Dec 2009
 by David A. Mellis
 
 */
#include <SPI.h>
#include <Ethernet.h>
#include <stdint.h>
#include <MemoryFree.h>

extern "C"{
  #include "jsonfind.h"
}
// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress server(216,52,233,121); 

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

prog_uchar requeststart[] PROGMEM  = {"{\"method\" : \"subscribe\","
                    "\"resource\" : \"/feeds/60405\","
                    "\"headers\" : {\"X-ApiKey\" : \"dv8eyuGVW3uWVts96m9HFc0YPbWSAKx6Q1B4ZTBZV0Zwbz0g\" },"
                    "\"token\" : \""};
prog_uchar requestend[] PROGMEM = {"\" "
                    "} "};

prog_uchar starting[] PROGMEM  = {"Starting up...\n"};
prog_uchar connecting[] PROGMEM  = {"Connecting...\n"};
prog_uchar failed_ethernet[] PROGMEM  = {"Failed to configure Ethernet using DHCP\n"};
prog_uchar connected_success[] PROGMEM  = {"Connected!\n"};
prog_uchar connected_failed[] PROGMEM  = {"Connection Failed :(\n"};
prog_uchar dumping_large[] PROGMEM  = {"Dumping Large packet: "};
prog_uchar free_memory[] PROGMEM  = {"Free Memory: "};
prog_uchar reading[] PROGMEM  = {"Reading Bytes: "};
prog_uchar disconnected[] PROGMEM  = {"Disconnected: "};

void SerialWrite(uint8_t val){
  Serial.write(val);
}

void EthWrite(uint8_t val){
  client.write(val);
}
#define SerialPrint_P(x) Print_P(x)
void Print_P(prog_uchar* str, void (*f)(uint8_t)= SerialWrite) {
  for (uint8_t c; (c = pgm_read_byte(str)); str++) (*f)(c);
}

// ETHERNET CLIENT
#define ClientPrint(x, y) SerialPrint_P(PSTR(x), y)

void printMemory(){
    SerialPrint_P(free_memory);
    Serial.println(freeMemory());
}

void setup() {
  // start the serial library:
  Serial.begin(57600);
  // start the Ethernet connection
  
  SerialPrint_P(starting);
  if (Ethernet.begin(mac) == 0) {
    SerialPrint_P(failed_ethernet);
    // no point in carrying on, so do nothing forevermore:
    for(;;)
      ;
  }
  
  randomSeed(analogRead(0));
  
  // give the Ethernet shield a second to initialize:
  delay(1000);
  SerialPrint_P(connecting);

  // if you get a connection, report back via serial:
  if (client.connect(server, 8081)) {
    SerialPrint_P(connected_success);
    // Make a HTTP request:

    long randNum = random();
    SerialPrint_P(requeststart);
    Serial.print(randNum);
    SerialPrint_P(requestend);
    Serial.println();
    Print_P(requeststart,EthWrite);
    client.print(randNum);
    Print_P(requestend,EthWrite);
    client.println();
  } 
  else {
    // kf you didn't get a connection to the server:
    SerialPrint_P(connected_failed);
  }
  printMemory();
}

#define BUFSIZ 1024
char buffer[BUFSIZ+1];


void loop()
{

  int bytes = client.available();
  if(bytes >BUFSIZ){
      SerialPrint_P(dumping_large);
      Serial.println(bytes);
      for(int i=0;i<bytes;i++)
        client.read();
  }
  else if(bytes){
    printMemory();
      
    SerialPrint_P(reading);
    
    Serial.println(bytes);
    for( int i=0;i<bytes;i++){
      buffer[i]=client.read();
    }
    
    buffer[bytes+1]=0;

    json_obj test;
    int result = json_find_parameter(buffer, "status", bytes, &test);
    Serial.println(result);
    
    Serial.println(buffer);
    memset(buffer,0,BUFSIZ+1);
  }
  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    SerialPrint_P(disconnected);
    client.stop();

    // do nothing forevermore:
    for(;;)
      ;
  }
}

