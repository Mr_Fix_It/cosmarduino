typedef struct {
        char* string;
        int name_index;
        int name_length;
        int argument_index;
        int argument_length;
} json_obj;

typedef enum {
        JSON_ERROR_NOT_FOUND = -1,
        JSON_ERROR_NOT_FOUND_INBOUNDS = -2,
        JSON_ERROR_PARAMS_NOT_FOUND = -3,
        JSON_ERROR_PARAMS_END_NOT_FOUND =-4,      
        JSON_SUCCESS = 0
} json_err;

json_err json_find_parameter(const char* string, const char* string_to_find, int length, json_obj* returned);
