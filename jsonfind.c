#include "jsonfind.h"
#include <stdlib.h>
#include "Arduino.h"
//fix to return enum instead of int

json_err json_find_parameter(const char* string, const char* string_to_find, int length, json_obj* returned){
   
   //prepare a copy of the string with extra characters in place
   //redo this to find the chars and check (aka avoid the malloc)
   int strlength= strlen(string_to_find)+3;
   char* buffer = (char*) malloc(strlength);
   buffer[0]='{';
   buffer[1]='"';
   strcpy(buffer+2,string_to_find);
   buffer[strlength-2]='"';
   buffer[strlength-1]=0;
   
   char* result = strstr(string,buffer);
   free(buffer);
   //couldn't find child string - error
   if(result==NULL){ return JSON_ERROR_NOT_FOUND; }
   //check length to see if within bounds
   if((int)(result-string) > length){ return JSON_ERROR_NOT_FOUND_INBOUNDS;}
   
   //else we found it inside the string
   char* params_start = strstr(":", result);
   if(params_start==NULL) {return JSON_ERROR_PARAMS_NOT_FOUND;}
   
   //find length of params - tricky
   
   //hack for now
   char* params_end = strstr("\",",params_start);
   if(params_end == NULL) {return JSON_ERROR_PARAMS_END_NOT_FOUND;}
   
   returned->string=string;
   returned->name_index=result+2;
   returned->name_length=strlength-3;
   returned->argument_index=params_start;
   returned->argument_length=(int)(params_end-params_start);
   
   return JSON_SUCCESS;
}
